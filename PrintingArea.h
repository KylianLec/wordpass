#ifndef PRINTINGAREA_H
#define PRINTINGAREA_H

#include <QString>
#include <QRect>

class PrintingArea
{
public:
    /* Constructors */
    PrintingArea();
    PrintingArea(QRect rect, QString text);

    /* Getters */
    QRect getZone();
    QString getText();

    /* Setters */
    void setZone(QRect zone);
    void setText(QString text);

private:
    /* Variables */
    QRect zone;
    QString text;
};

#endif // PRINTINGAREA_H
