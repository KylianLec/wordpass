#ifndef ENCRYPTION_H
#define ENCRYPTION_H

#include <iostream>
#include <string>

#include <QString>

class Encryption
{
public:
    Encryption();
    static QString encrypt(QString str);
    static QString decrypt(QString str);
};

#endif // ENCRYPTION_H
