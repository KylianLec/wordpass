#include "FileManager.h"

FileManager::FileManager()
{

}

/* Static Functions */
void FileManager::save(QFile * file, QVector<Account *> accounts)
{
    if ( !file->open(QIODevice::WriteOnly) )
    {
        std::cerr << "Error::write : Cannoooot open file " << qPrintable(file->fileName())
                  << ": " << qPrintable(file->errorString())
                  << std::endl;
    }

    QXmlStreamWriter xmlWriter(file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("racine");
    if(accounts.size() > 0)
    {
        for(int i = 0 ; i < accounts.size() ; i++)
        {
            Account * currentAcc = accounts.at(i);
            xmlWriter.writeStartElement("account");

            QVector<Field *> currFields = currentAcc->getFields();
            for(int i = 0 ; i < currFields.size() ; i++)
            {
                xmlWriter.writeStartElement(currFields.at(i)->getTitle());
                xmlWriter.writeAttribute("hidden", currFields.at(i)->getHidden() ? "true" : "false");
                xmlWriter.writeCharacters(Encryption::encrypt(currFields.at(i)->getContent()));
                xmlWriter.writeEndElement();
            }

            xmlWriter.writeEndElement();//End Account
        }
    }
    xmlWriter.writeEndElement();//End racine
    file->close();

    if (xmlWriter.hasError())
    {
       std::cerr << "Error::writerend: Failed to parse file "
                 << qPrintable(file->fileName()) << ": "
                 << qPrintable(xmlWriter.hasError()) << std::endl;
    }
    else if (file->error() != QFile::NoError)
    {
        std::cerr << "Error::writerend: Cannot write file " << qPrintable(file->fileName())
                  << ": " << qPrintable(file->errorString())
                  << std::endl;
    }
}

QVector<Account*> FileManager::load(QFile * file)
{
    QDomDocument xmlBOM;

    if ( !file->open(QFile::ReadOnly | QFile::Text) )
    {
        std::cerr << "Error::read : Cannot read file " << qPrintable(file->fileName())
                  << ": " << qPrintable(file->errorString())
                  << std::endl;
    }
    if(!xmlBOM.setContent(file))
    {
        std::cerr << "Error::setting : Cannot set the content " << std::endl;
        file->close();
    }
    file->close();

    QDomElement root = xmlBOM.documentElement();
    QDomElement component = root.firstChild().toElement();

    QVector<Account *> accounts;
    while(!component.isNull())
    {
        if(component.tagName() == "account")
        {
            QDomElement child = component.firstChild().toElement();

            QVector<Field *> fields;
            while(!child.isNull())
            {
                bool hidden = child.attribute("hidden") == "true";
                fields.push_back(new Field(child.tagName(), Encryption::decrypt(child.text()), hidden));
                child = child.nextSibling().toElement();
            }
            //accounts.push_back(new Account(fields));
            accounts = sortingAdd(accounts, new Account(fields));
        }
        component = component.nextSibling().toElement();
    }

    return accounts;
}

QVector<Account *> FileManager::sortingAdd(QVector<Account *> accounts, Account * account)
{
    if( accounts.size() == 0 ) accounts.push_back(account);
    else
    {
        int i = 0;
        QString content;
        QString comparedWord;
        bool ok = false;

        while(i < accounts.size() && ok == false)
        {
            content = account->getFields().at(0)->getContent();
            comparedWord = accounts.at(i)->getFields().at(0)->getContent();

            //si le compte doit être placé avant celui à qui il est comparé
            if(!FileManager::isAfterInAlphabet(content , comparedWord))
            {
                accounts.insert(i, account);
                ok = true;
            }
            i++;
        }

        if(!ok) accounts.push_back(account);
    }
    return accounts;
}

bool FileManager::isAfterInAlphabet(QString current, QString comparedWord)
{
    int i = 0;

    while(current.at(i) == comparedWord.at(i) && i < current.size()-1 && i < comparedWord.size()-1)
    {
        i++;
    }
    if( current.at(i) <= comparedWord.at(i)) return false;
    else return true;
}



