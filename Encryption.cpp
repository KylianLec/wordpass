#include "Encryption.h"

Encryption::Encryption()
{
}

QString Encryption::encrypt(QString str)
{
    std::string strTmp = str.toStdString();
    std::string cryptedString = "";

    for(std::string::iterator it=strTmp.begin() ; it != strTmp.end() ; ++it)
    {
        cryptedString += *it+43;
    }

    return QString::fromStdString(cryptedString);
}

QString Encryption::decrypt(QString str)
{
    std::string strTmp = str.toStdString();
    std::string decryptedString = "";
    for(std::string::iterator it=strTmp.begin() ; it != strTmp.end() ; ++it)
    {
        decryptedString += *it-43;
    }

    return QString::fromStdString(decryptedString);
}
