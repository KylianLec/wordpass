#ifndef NOUVEAUCOMPTEDIALOG_H
#define NOUVEAUCOMPTEDIALOG_H

#include "Account.h"

#include <QDialog>
#include <QMessageBox>
#include <QRegExpValidator>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QCheckBox>
#include <QSignalMapper>

#include <iostream>

#include "NewAccountField.h"

namespace Ui {
class NouveauCompteDialog;
}

class NouveauCompteDialog : public QDialog
{
    Q_OBJECT

public:
    /* Constructors and destructors */
    explicit NouveauCompteDialog(QWidget *parent = 0);
    NouveauCompteDialog(Account * account);
    ~NouveauCompteDialog();

    /* Getters */
    QVector<Field *> getFields() const;

private slots:
    /* Slots */
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_addButton_clicked();
    void on_whatButton_clicked();

    //void remove(QObject * object);

private:
    /* Functions */
    bool contentAccepted();
    bool titleFieldAccepted();

    /* Forms */
    Ui::NouveauCompteDialog *ui;

    /* Variables */
    QVector<NewAccountField *> newAccountFields;

    //QSignalMapper * mapper;
};

#endif // NOUVEAUCOMPTEDIALOG_H
