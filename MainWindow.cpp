#include "MainWindow.h"
#include "ui_MainWindow.h"

/* Constructeurs et Destructeurs */
MainWindow::MainWindow()
    : QMainWindow()
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->initVariables();
    this->initMenu();
    this->updateDisplay();

    if (FileManager::isAfterInAlphabet("bonjour", "aurevoir")) std::cout << "true" << std::endl;
    else std::cout << "false" << std::endl;

    if (FileManager::isAfterInAlphabet("aurevoir", "aurevoir")) std::cout << "true" << std::endl;
    else std::cout << "false" << std::endl;

    if (FileManager::isAfterInAlphabet("abhzjp", "abhzja")) std::cout << "true" << std::endl;
    else std::cout << "false" << std::endl;

    if (FileManager::isAfterInAlphabet("abhzja", "abhzjp")) std::cout << "true" << std::endl;
    else std::cout << "false" << std::endl;
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Private Slots */
void MainWindow::on_newAccount_clicked()
{
    NouveauCompteDialog ncd;
    int res = ncd.exec();

    if(res == QDialog::Rejected)
        return;

    //Create and store the new account
    Account * newAccount = new Account(ncd.getFields());
    this->accounts.push_back(newAccount);

    this->setWidgetAccount(newAccount);

    //Add the widget of the new account to the stack of widget
    this->ui->stackBotContainer->addWidget(this->widgetOfAccount.value(newAccount));

    //Update display with the new account
    this->updateDisplay();

    //Save the profile
    FileManager::save(this->file, this->accounts);
}

void MainWindow::on_editAccount_clicked()
{
    //If there is no line selected, create an error message
    if(this->observedAccount == 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Il faut selectionner un compte ");
        msgBox.exec();
    }
    //If there is one line selected
    else
    {
        //Create a new account dialog to edit the account
        NouveauCompteDialog ncd(this->observedAccount);
        int res = ncd.exec();

        if(res == QDialog::Rejected)
            return;

        QVector<Field *> newField = ncd.getFields();

        if(newField.size() > this->observedAccount->getFields().size())
        {
            int nbNewField = newField.size()-this->observedAccount->getFields().size();
            for(int i = 0 ; i < nbNewField ; i++) this->observedAccount->addField(new Field("", ""));
        }

        //Set news informations to the account
        for(int i = 0 ; i < newField.size() ; i++)
        {
            this->observedAccount->getFields().at(i)->setTitle(newField.at(i)->getTitle());
            this->observedAccount->getFields().at(i)->setContent(newField.at(i)->getContent());
        }

        //Update
        this->updateDisplay();

        //Save the profile
        FileManager::save(this->file, this->accounts);
    }
}

void MainWindow::on_deleteAccount_clicked()
{

    //Get the selected account
    QList<QTableWidgetItem *> list = ui->listAccount->selectedItems();

    //If there is only the observed account
    if(list.size() == 0 && this->observedAccount != NULL)
    {
        this->accounts.remove(this->accounts.indexOf(this->observedAccount));
    }
    else if(list.size() != 0)
    {
        //Delete the selected account in the profile and in the table
        for(int i=0 ; i < list.size() ; i++)
        {
            int row = list[i]->row();
            this->accounts.remove(row);
            ui->listAccount->removeRow(row);
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Vous devez au moins selectionner un compte a supprimer");
        msgBox.exec();
    }

    //Update the disply
    this->updateDisplay();

    //Save the profile
    FileManager::save(this->file, this->accounts);
}

void MainWindow::on_print_clicked()
{
    QPrinter * printer = new QPrinter();
    QPainter * painter = new QPainter();
    QPrintDialog pd(this);

    if (pd.exec() == QDialog::Rejected) return;
    printer = pd.printer();

    if(!painter->begin(printer))
    {
        qWarning("Failed to open file, is it writable ?");
        return;
    }

    int nbAccount = this->accounts.size();

    float nbLine = nbAccount/4;
    if(nbAccount%4 != 0) nbLine++;

    //On récupère la hauteur pour chaque ligne
    QVector<int> heights = QVector<int>();

    //Tableau 2D qui contiendra toutes les zones de textes
    QVector<QVector<PrintingArea *> > rectangles = QVector<QVector<PrintingArea *> >();


    const int leftRightSpaceBetween = 10;
    const int topDownSpaceBetween = 10;
    const int width = 175;
    int posX = 0;
    int posY = 0;

    int nbElementByLine = 4;
    for(int i = 0 ; i < nbLine ; i++)
    {
        //Si c'est la dernière ligne et qu'il y a un nombre d'élément différent de 4
        if(i == nbLine-1 && nbAccount%4 != 0) nbElementByLine = nbAccount%4;

        //Calcul la hauteur du champ le plus grand
        int maxNbField = 0;
        for (int j = 0 ; j < nbElementByLine ; j++)//1 ligne possède 4 comptes
        {
            if(this->accounts.at(j+(4*i))->getNbField() > maxNbField)
                maxNbField = this->accounts.at(j+(4*i))->getNbField();//Le nombre max de champ
        }
        heights.push_back(15*maxNbField);

        rectangles.push_back(QVector<PrintingArea *>());

        if(i==0) posY = topDownSpaceBetween; //Premier élément
        else posY += topDownSpaceBetween + heights.at(i-1); //Les autres éléments

        if(posY + heights.at(i) > 1055)
        {
            posY = topDownSpaceBetween;//Overflow
        }

        for(int j = 0 ; j < nbElementByLine ; j++)
        {
            posX = ( leftRightSpaceBetween * (j+1) ) + ( width * j );
            rectangles[i].push_back(new PrintingArea(QRect(posX, posY, width, heights.at(i)), this->accounts.at(j+(i*4))->toString()));
        }
    }

    //Draw accounts
    for(int i = 0 ; i < rectangles.size() ; i++)
    {
        for(int j = 0 ; j < rectangles[i].size() ; j++)
        {
            if(i != 0 && rectangles[i][j]->getZone().y() == topDownSpaceBetween && rectangles[i][j]->getZone().x() == leftRightSpaceBetween)
                printer->newPage();

            painter->drawRect(rectangles[i][j]->getZone());
            painter->drawText(rectangles[i][j]->getZone(), Qt::AlignHCenter, rectangles[i][j]->getText());
        }
    }

    delete painter;
    delete printer;
}

void MainWindow::on_quit_clicked()
{
    //Save the profile
    FileManager::save(this->file, this->accounts);

    //Quit the program
    qApp->quit();
}

void MainWindow::on_listAccount_cellClicked(const int row)
{
    this->updateDisplay();

    this->observedAccount = accounts[row];

    std::cout << this->observedAccount->toString().toStdString() << std::endl;

    //Update account widget
    this->setWidgetAccount(this->observedAccount);

    //Update account widget in the stack
    this->ui->stackBotContainer->removeWidget(this->widgetOfAccount.value(this->observedAccount));
    this->ui->stackBotContainer->addWidget(this->widgetOfAccount.value(this->observedAccount));

    //Display the selected account
    this->ui->stackBotContainer->setCurrentWidget(this->widgetOfAccount.value(this->observedAccount));
}

void MainWindow::copyContent(const QString &content)
{
    QClipboard *pressePapiers = QApplication::clipboard();
    pressePapiers->setText(content);
}

void MainWindow::hideField(QWidget *label)
{
    HiddenLabel * qLabel = new HiddenLabel("");
    qLabel = static_cast<HiddenLabel *>(label);

    qLabel->text() == "*****" ? qLabel->setText(qLabel->getLabelString()) : qLabel->setText("*****");

}

/* Private function */
void MainWindow::initVariables()
{
    this->alphabeticalOrder = false;
    this->dir = new QDir();
    this->dir->mkpath("./save");

    //Init the file
    this->file = new QFile("./save/save.xml");

    //The save file is already created
    if(this->file->exists() && this->file->size() != 0)
    {
        this->accounts = FileManager::load(this->file);
        //this->alphabeticalOrder = true;
    }
    else //There is no save file
    {
        this->file->open(QIODevice::WriteOnly);
        this->file->close();
    }

    this->mapperCopy = new QSignalMapper();
    QObject::connect(this->mapperCopy, SIGNAL(mapped(const QString &)), this, SLOT(copyContent(const QString &)));

    this->mapperHide = new QSignalMapper();
    QObject::connect(this->mapperHide, SIGNAL(mapped(QWidget *)), this, SLOT(hideField(QWidget *)));
}

void MainWindow::initMenu()
{
    fileMenu = menuBar()->addMenu("Fichier");

    newAccountAct = new QAction("Nouveau", this);
    newAccountAct->setShortcut(QKeySequence("Ctrl+N"));
    connect(newAccountAct, SIGNAL(triggered()), this, SLOT(on_newAccount_clicked()));
    fileMenu->addAction(newAccountAct);

    editAct = new QAction("Edition", this);
    editAct->setShortcut(QKeySequence("Ctrl+E"));
    connect(editAct, SIGNAL(triggered()), this, SLOT(on_editAccount_clicked()));
    fileMenu->addAction(editAct);

    deleteAct = new QAction("Supprimer", this);
    deleteAct->setShortcut(QKeySequence("Del"));
    connect(deleteAct, SIGNAL(triggered()), this, SLOT(on_deleteAccount_clicked()));
    fileMenu->addAction(deleteAct);

    printAct = new QAction("Imprimer", this);
    printAct->setShortcut(QKeySequence("Ctrl+P"));
    connect(printAct, SIGNAL(triggered()), this, SLOT(on_print_clicked()));
    fileMenu->addAction(printAct);

    quitAct = new QAction("Quitter", this);
    quitAct->setShortcut(QKeySequence("Ctrl+Q"));
    connect(quitAct, SIGNAL(triggered()), this, SLOT(on_quit_clicked()));
    fileMenu->addAction(quitAct);
}

void MainWindow::updateDisplay()
{
    this->observedAccount = NULL;
    this->ui->stackBotContainer->setCurrentIndex(0);

    //Reset the list on the left side
    ui->listAccount->setRowCount(0);

    //Fill the TableWdiget with the news accounts
    int nbAccounts = this->accounts.size();
    for(int i = 0 ; i < nbAccounts ; i++)
    {
        Account tmp = *this->accounts[i];
        ui->listAccount->insertRow(ui->listAccount->rowCount());
        int nbLine = ui->listAccount->rowCount()-1;

        ui->listAccount->setItem(nbLine, WEBNAME, new QTableWidgetItem(tmp.getFields().at(0)->getContent()));
        ui->listAccount->setItem(nbLine, EMAIL, new QTableWidgetItem(tmp.getFields().at(1)->getContent()));
    }
}

void MainWindow::setWidgetAccount(Account * account)
{
    QVector<Field *> fields = account->getFields();
    QWidget * widget = new QWidget();

    QVBoxLayout * layout = new QVBoxLayout();

    for(int i = 0 ; i < fields.size() ; i++)
    {
        QHBoxLayout * sublayout = new QHBoxLayout;

        QSpacerItem * spacer = new QSpacerItem(36, 20, QSizePolicy::Fixed);

        QLabel * title = new QLabel(fields.at(i)->getTitle());
        QFont font = title->font();
        font.setPointSize(12);
        title->setFont(font);
        title->setStyleSheet("border: 1px solid black");

        HiddenLabel * content = new HiddenLabel(fields.at(i)->getContent());
        content->setStyleSheet("border: 1px solid black");

        QPushButton * copyButton = new QPushButton("Copier");
        copyButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        copyButton->setMaximumSize(QSize(50, 22));

        QObject::connect(copyButton, SIGNAL(clicked()), this->mapperCopy, SLOT(map()));
        this->mapperCopy->setMapping(copyButton, fields.at(i)->getContent());


        if(fields.at(i)->getHidden())
        {
            QPushButton * hiddenButton = new QPushButton(QIcon(":/data/oeil.png"), "");
            hiddenButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            hiddenButton->setMaximumSize(QSize(25, 22));

            QObject::connect(hiddenButton, SIGNAL(clicked()), this->mapperHide, SLOT(map()));
            this->mapperHide->setMapping(hiddenButton, content);
            sublayout->addWidget(hiddenButton);
        }
        else sublayout->addItem(spacer);

        sublayout->addWidget(title);
        sublayout->addWidget(content);
        sublayout->addWidget(copyButton);

        layout->addLayout(sublayout);
    }
    widget->setLayout(layout);
    widgetOfAccount.remove(account);
    widgetOfAccount.insert(account, widget);
}
