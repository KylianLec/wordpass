#include "Field.h"

Field::Field()
{

}

Field::Field(QString title, QString content, bool hidden)
{
    this->title = title;
    this->content = content;
    this->hidden = hidden;
}

/* Getters */
QString Field::getTitle(){ return this->title; }
QString Field::getContent(){ return this->content; }
bool Field::getHidden(){ return this->hidden; }

/* Setters */
void Field::setTitle(QString title){ this->title = title; }
void Field::setContent(QString content){ this->content = content; }
void Field::setHidden(bool hidden){ this->hidden = hidden; }
