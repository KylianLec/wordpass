#ifndef HIDDENLABEL_H
#define HIDDENLABEL_H

#include <QLabel>

class HiddenLabel : public QLabel
{
public:
    HiddenLabel(QString labelString);

    QString getLabelString();

    void setLabelString(QString labelString);

private:
    QString labelString;
};

#endif // HIDDENLABEL_H
