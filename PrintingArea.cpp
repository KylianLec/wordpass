#include "PrintingArea.h"

/* Constructors */
PrintingArea::PrintingArea(){}
PrintingArea::PrintingArea(QRect zone, QString text)
{
    this->zone = zone;
    this->text = text;
}

/* Getters */
QRect PrintingArea::getZone(){ return this->zone; }
QString PrintingArea::getText(){ return this->text; }

/* Setters */
void PrintingArea::setZone(QRect zone){ this->zone = zone; }
void PrintingArea::setText(QString text){ this->text = text; }
