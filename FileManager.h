#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QFile>
#include <QDebug>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QFileDialog>
#include <QByteArray>
#include <QtXml>

#include <iostream>
#include <functional>
#include <string>

#include "Account.h"
#include "Encryption.h"

class FileManager
{
public:
    /* Constructors */
    FileManager();

    /* Functions */
    static void save(QFile * file, QVector<Account *> accounts);
    static QVector<Account*> load(QFile * file);

    static QVector<Account *> sortingAdd(QVector<Account *> accounts, Account * account);
    static bool isAfterInAlphabet(QString current, QString comparedWord);
};

#endif // FILEMANAGER_H
