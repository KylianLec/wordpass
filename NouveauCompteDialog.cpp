#include "NouveauCompteDialog.h"
#include "ui_NouveauCompteDialog.h"


/* Constructors and destructors */
NouveauCompteDialog::NouveauCompteDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NouveauCompteDialog)
{
    ui->setupUi(this);

    //this->mapper = new QSignalMapper();
    //QObject::connect(this->mapper, SIGNAL(mapped(QObject *)), this, SLOT(remove(QObject *)));

}

NouveauCompteDialog::NouveauCompteDialog(Account * account) :
    ui(new Ui::NouveauCompteDialog)
{
    ui->setupUi(this);

    QVector<Field *> fieldsTmp = account->getFields();
    int nbField = fieldsTmp.size();

    ui->webName->setText(fieldsTmp.at(0)->getContent());
    ui->eMail->setText(fieldsTmp.at(1)->getContent());
    ui->password->setText(fieldsTmp.at(2)->getContent());

    if(nbField > 3)
    {
        for(int i = 3 ; i < nbField ; i++)
        {
            NewAccountField * naf = new NewAccountField(fieldsTmp.at(i));
            this->newAccountFields.push_back(naf);

            ui->containerFields->addLayout(naf);

            //QObject::connect(naf->getRemove(), SIGNAL(clicked()), this->mapper, SLOT(map()));
            //this->mapper->setMapping(naf->getRemove(), naf);
        }
    }
}

NouveauCompteDialog::~NouveauCompteDialog()
{
    delete ui;
}

/* Getters */
QVector<Field *> NouveauCompteDialog::getFields() const
{
    QVector<Field *> fields = QVector<Field *>();
    fields.append(new Field("webname", ui->webName->text(), false));
    fields.append(new Field("email", ui->eMail->text(), false));
    fields.append(new Field("password", ui->password->text(), true));

    for(int i = 0 ; i < this->newAccountFields.size() ; i++)
    {
        if(this->newAccountFields.at(i)->getTitle()->text() != "" &&
                this->newAccountFields.at(i)->getContent()->text() != "")
        {
            fields.push_back(new Field(this->newAccountFields.at(i)->getTitle()->text(),
                                       this->newAccountFields.at(i)->getContent()->text(),
                                       this->newAccountFields.at(i)->getHidden()->isChecked()));
        }
    }

    return fields;
}

/* Functions */
bool NouveauCompteDialog::contentAccepted()
{
    QRegExp rx("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$");
    QRegExpValidator v(rx);

    QString s = ui->eMail->text();
    int pos = 0;

    if(!ui->webName->text().isEmpty() &&
        !ui->password->text().isEmpty() &&
        !ui->eMail->text().isEmpty() &&
        v.validate(s, pos) == QValidator::Acceptable)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool NouveauCompteDialog::titleFieldAccepted()
{
    if(this->newAccountFields.size() != 0)
    {
        QString alphabet("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        for(int i = 0 ; i < this->newAccountFields.size() ; i++)
        {
            QString testingTitle = this->newAccountFields.at(i)->getTitle()->text();

            for(int j = 0 ; j < testingTitle.size() ; j++)
                if(!alphabet.contains(testingTitle.at(j))) return false;
        }
    }

    return true;
}

/* Slots */
void NouveauCompteDialog::on_buttonBox_accepted()
{
    if(!this->contentAccepted())
    {
        QMessageBox msgBox;
        msgBox.setText("Erreur dans l'adresse mail");
        msgBox.exec();
    }
    else
    {
        if(!this->titleFieldAccepted())
        {
            QMessageBox msgBox;
            msgBox.setText("Erreur dans un des titres des champs personnalises (champ de gauche)");;
            msgBox.exec();
        }
        else
        {
            accept();
        }
    }
}

void NouveauCompteDialog::on_buttonBox_rejected()
{
    rejected();
    this->close();
}

//Add new generic field
void NouveauCompteDialog::on_addButton_clicked()
{
    NewAccountField * naf = new NewAccountField();
    this->newAccountFields.push_back(naf);

    ui->containerFields->addLayout(naf);

    //QObject::connect(naf->getRemove(), SIGNAL(clicked()), this->mapper, SLOT(map()));
    //this->mapper->setMapping(naf->getRemove(), naf);
}

void NouveauCompteDialog::on_whatButton_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Le bouton '+' vous permet de rajouter un champ personnalise\n"
                   "- a gauche l'intitule du champ (ex. Telephone)\n"
                   "- a droite le contenu (ex. 06 95 48 78 52)\n"
                   "La case 'cache' a droite permet d'ajouter un bouton pour cacher le champ");
    msgBox.exec();
}

/*
void NouveauCompteDialog::remove(QObject * layout)
{
    std::cout << "bonjour" << std::endl;
    NewAccountField * naf = new NewAccountField();
    naf = static_cast<NewAccountField *>(layout);

    this->newAccountFields.remove(this->newAccountFields.indexOf(naf));
    this->ui->containerFields->removeItem(naf);
}
*/
