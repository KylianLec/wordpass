#include "Account.h"

/* Constructors */
Account::Account(){}

Account::Account(QVector<Field *> fields)
{
    this->fields = fields;
    this->nbField = this->fields.size();
}

/* Getters */
QVector<Field *> Account::getFields() { return this->fields; }
int Account::getNbField(){ return this->nbField; }

/* Setters */
void Account::setFields(QVector<Field *> fields) { this->fields = fields; }

/* Functions */
QString Account::toString()
{
    QString description = "";
    for(int i  = 0 ; i < this->fields.size() ; i++)
    {
        description += this->fields.at(i)->getTitle() + " : " + this->fields.at(i)->getContent() + "\n";
    }
    return description;
}

void Account::addField(QString title, QString content){
    this->fields.push_back(new Field(title, content));
    this->nbField++;
}

void Account::addField(Field * field){
    this->fields.push_back(field);
    this->nbField++;
}

