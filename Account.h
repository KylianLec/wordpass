#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QString>
#include <QVector>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QSignalMapper>
#include <QWidget>

#include <iostream>

#include "Field.h"

class Account
{

public:
    /* Constructors */
    Account();
    Account(QVector<Field *> fields);

    /* Getters */
    QVector<Field *> getFields();
    int getNbField();

    /* Setters */
    void setFields(QVector<Field *> webName);

    /* Functions */
    QString toString();
    void addField(QString title, QString content);
    void addField(Field * field);

private:
    /* Variables */
    QVector<Field *> fields;
    int nbField;

};

#endif // ACCOUNT_H
