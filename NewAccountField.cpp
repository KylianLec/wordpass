#include "NewAccountField.h"

NewAccountField::NewAccountField(Field * field)
{
    this->remove = new QPushButton("-");
    this->remove->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    this->remove->setMaximumSize(QSize(22, 22));

    this->title = new QLineEdit();
    this->content = new QLineEdit();
    this->hidden = new QCheckBox("cache ?");

    if ( field != 0 )
    {
        this->title->setText(field->getTitle());
        this->content->setText(field->getContent());
        this->hidden->setChecked(field->getHidden());
    }

    //this->addWidget(this->remove); //Non fonctionnel
    this->addWidget(this->title);
    this->addWidget(this->content);
    this->addWidget(this->hidden);
}

/* Getters */
QPushButton * NewAccountField::getRemove() { return this->remove; }
QLineEdit * NewAccountField::getTitle() { return this->title; }
QLineEdit * NewAccountField::getContent() { return this->content; }
QCheckBox * NewAccountField::getHidden() { return this->hidden; }

/* Setters */
void NewAccountField::setRemove(QPushButton * remove) { this->remove = remove; }
void NewAccountField::setTitle(QLineEdit * title) { this->title = title; }
void NewAccountField::setContent(QLineEdit * content) { this->content = content; }
void NewAccountField::setHidden(QCheckBox * hidden) { this->hidden = hidden; }


