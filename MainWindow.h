#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <cmath>

#include <QTableWidgetItem>
#include <QMainWindow>
#include <QVector>
#include <QList>
#include <QMimeData>
#include <QClipboard>
#include <QPrintDialog>
#include <QPrinter>
#include <QPainter>
#include <QSpacerItem>

#include "Account.h"
#include "Profile.h"
#include "FileManager.h"
#include "NouveauCompteDialog.h"
#include "PrintingArea.h"
#include "HiddenLabel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /* Constructors et destructors */
    MainWindow();
    ~MainWindow();

private slots:
    /* Private Slots */
    void on_newAccount_clicked();
    void on_editAccount_clicked();
    void on_deleteAccount_clicked();
    void on_print_clicked();
    void on_quit_clicked();
    void on_listAccount_cellClicked(const int row);

    void copyContent(const QString &);
    void hideField(QWidget *);

private:
    /* Forms */
    Ui::MainWindow *ui;

    /* Variables */
    QVector<Account*> accounts;
    Account * observedAccount;
    QMap<Account *, QWidget *> widgetOfAccount;

    QDir * dir;
    QFile * file;

    QSignalMapper * mapperCopy;
    QSignalMapper * mapperHide;

    bool alphabeticalOrder;

    /* QMenu variables */
    QMenu * fileMenu;

    QAction * newAccountAct;
    QAction * editAct;
    QAction * deleteAct;
    QAction * printAct;
    QAction * quitAct;

    /* Fonctions */
    void initVariables();
    void initMenu();
    void updateDisplay();
    void setWidgetAccount(Account * account);

    /* Enumérations */
    enum Colonne { WEBNAME = 0, EMAIL };

};
#endif // MAINWINDOW_H
