#include "HiddenLabel.h"

HiddenLabel::HiddenLabel(QString labelString)
{
    this->setText(labelString);
    this->labelString = labelString;
}

QString HiddenLabel::getLabelString(){ return this->labelString; }

void HiddenLabel::setLabelString(QString labelString) { this->labelString = labelString; }
