#ifndef FIELD_H
#define FIELD_H

#include <QString>

class Field
{
public:
    /* Constructors */
    Field();
    Field(QString title, QString content, bool hidden = false);

    /* Getters */
    QString getTitle();
    QString getContent();
    bool getHidden();

    /* Setters */
    void setTitle(QString title);
    void setContent(QString content);
    void setHidden(bool hidden);

private:
    /* Variables */
    QString title;
    QString content;
    bool hidden;
};

#endif // FIELD_H
