#ifndef NEWACCOUNTFIELD_H
#define NEWACCOUNTFIELD_H

#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QCheckBox>
#include <QLabel>

#include <iostream>

#include "Field.h"

class NewAccountField : public QHBoxLayout
{
public:
    NewAccountField(Field * field = 0);

    /* Getters */
    QPushButton * getRemove();
    QLineEdit * getTitle();
    QLineEdit * getContent();
    QCheckBox * getHidden();

    /* Setters */
    void setRemove(QPushButton *);
    void setTitle(QLineEdit *);
    void setContent(QLineEdit *);
    void setHidden(QCheckBox *);

private:
    QPushButton * remove;
    QLineEdit * title;
    QLineEdit * content;
    QCheckBox * hidden;

};

#endif // NEWACCOUNTFIELD_H
